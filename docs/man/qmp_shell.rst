.. manpage-only doc; do not include in toctree.

:orphan:

qmp-shell
=========

SYNOPSIS
--------

qmp-shell [-h] [-H] [-v] [-p] [-l LOGFILE] [-N] qmp_server

DESCRIPTION
-----------

.. automodule:: qemu.qmp.qmp_shell
   :noindex:
   :trim-summary:
   :trim-usage:
