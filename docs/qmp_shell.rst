.. highlight:: console

.. _qmp-shell:

qmp-shell
=========

.. automodule:: qemu.qmp.qmp_shell
   :noindex:
