# Python library build & testing environment.

# Fedora is convenient, as it allows us to easily access multiple
# versions of the python interpreter, which is great for tox testing.
FROM fedora:rawhide

# 「はじめまして！」
MAINTAINER John Snow <jsnow@redhat.com>

# Please keep the packages sorted alphabetically.
RUN dnf --setopt=install_weak_deps=False install -y \
        gcc \
        git \
        make \
        python3 \
        python3-pip \
        python3-tox \
        python3-virtualenv \
        python3-wheel \
        python3.12 \
    && python3 -m pip install --upgrade \
        setuptools_scm \
    && dnf clean all \
    && rm -rf ~/.cache/pip \
    && rm -rf /var/cache/dnf \
    ;
